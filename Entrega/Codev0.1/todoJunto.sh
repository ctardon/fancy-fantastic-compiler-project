#!/bin/bash
#lexico
java -cp ../jlex.jar JLex.Main ./alex/AnalizadorLexicoExp
#sintactico
cd constructorast
java -cp ../../cup.jar java_cup.Main -parser ConstructorASTExp -symbols ClaseLexica -nopositions ConstructorAST.cup
cd ..

#Pasar analizadores y generar el codigo
rm generaCode/main.wat
javac -cp "../cup.jar" -Xlint:-unchecked */*.java 
if [ $# -eq 0 ]
  then
    echo "El primer argumento del script indica el fichero a compilar: ./todoJunto.sh nombreModulo.cc"
  else
    java -cp ".:../cup.jar" constructorast.Main $1
    # ejecutar 
    cd generaCode
    ./ejecuta.sh
fi

find . -name "*.class" -type f -delete

