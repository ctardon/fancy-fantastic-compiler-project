(module
(type $_sig_i32i32i32 (func (param i32 i32 i32) ))
(type $_sig_i32ri32 (func (param i32) (result i32)))
(type $_sig_i32 (func (param i32)))
(type $_sig_ri32 (func (result i32)))
(type $_sig_void (func ))
(import "runtime" "exceptionHandler" (func $exception (type $_sig_i32)))
(import "runtime" "print" (func $print (type $_sig_i32)))
(import "runtime" "read" (func $read (type $_sig_ri32)))
(memory 2000)
(global $SP (mut i32) (i32.const 0)) ;; start of stack
(global $MP (mut i32) (i32.const 0)) ;; mark pointer
(global $NP (mut i32) (i32.const 131071996)) ;; heap 2000*64*1024-4
(start $main)
(func $reserveStack (param $size i32)
   (result i32)
   get_global $MP
   get_global $SP
   set_global $MP
   get_global $SP
   get_local $size
   i32.add
   set_global $SP
   get_global $SP
   get_global $NP
   i32.gt_u
   if
   i32.const 3
   call $exception
   end
)
(func $freeStack (type $_sig_void)
   get_global $MP
   i32.load
   i32.load offset=4
   set_global $SP
   get_global $MP
   i32.load
   set_global $MP 
)
(func $mulMat (type $_sig_ri32) (result i32)
    (local $temp i32)
 (local $temp2 i32)
 (local $temp3 i32)
  (local $localsStart i32)
   i32.const 132
 call $reserveStack  ;; returns old MP (dynamic link)
    set_local $temp
    get_global $MP
    get_local $temp
    i32.store
    get_global $MP
    get_global $SP
    i32.store offset=4
    get_global $MP
    i32.const 8
    i32.add
    set_local $localsStart

    ;;Declaracion sin lado derecho
i32.const 108
get_local $localsStart 
i32.add
i32.const 0
i32.store     ;;Declaracion
block $fin
loop  $init ;;For
i32.const 108
get_local $localsStart 
i32.add
i32.load 
i32.const 3
i32.lt_s 
i32.eqz
br_if 1
i32.const 112
get_local $localsStart 
i32.add
i32.const 0
i32.store     ;;Declaracion
block $fin
loop  $init ;;For
i32.const 112
get_local $localsStart 
i32.add
i32.load 
i32.const 3
i32.lt_s 
i32.eqz
br_if 1
i32.const 72
get_local $localsStart 
i32.add
i32.const 12
i32.const 108
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 112
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 0
i32.store     ;;Asignacion
i32.const 116
get_local $localsStart 
i32.add
i32.const 0
i32.store     ;;Declaracion
block $fin
loop  $init ;;For
i32.const 116
get_local $localsStart 
i32.add
i32.load 
i32.const 3
i32.lt_s 
i32.eqz
br_if 1
i32.const 72
get_local $localsStart 
i32.add
i32.const 12
i32.const 108
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 112
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 72
get_local $localsStart 
i32.add
i32.const 12
i32.const 108
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 112
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.load 
i32.const 0
get_local $localsStart 
i32.add
i32.const 12
i32.const 108
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 116
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.load 
i32.const 36
get_local $localsStart 
i32.add
i32.const 12
i32.const 116
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 112
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.load 
i32.mul 
i32.add 
i32.store     ;;Asignacion
i32.const 116
get_local $localsStart 
i32.add
i32.const 116
get_local $localsStart 
i32.add
i32.load 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 116
get_local $localsStart 
i32.add
i32.load 
set_local $temp3
br 0
end ;;finFor
end
i32.const 112
get_local $localsStart 
i32.add
i32.const 112
get_local $localsStart 
i32.add
i32.load 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 112
get_local $localsStart 
i32.add
i32.load 
set_local $temp3
br 0
end ;;finFor
end
i32.const 108
get_local $localsStart 
i32.add
i32.const 108
get_local $localsStart 
i32.add
i32.load 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 108
get_local $localsStart 
i32.add
i32.load 
set_local $temp3
br 0
end ;;finFor
end
i32.const 108
get_local $localsStart 
i32.add
i32.const 72
get_local $localsStart 
i32.add
i32.store     ;;Declaracion
i32.const 108
get_local $localsStart 
i32.add
i32.load 
return  ;;return

 i32.const -1 
 call $freeStack)
(func $MatMult_multMat (type $_sig_i32ri32)
 (param $this i32) (result i32)
    (local $temp i32)
 (local $temp2 i32)
 (local $temp3 i32)
 (local $localsStart i32)
   i32.const 24
 call $reserveStack  ;; returns old MP (dynamic link)
    set_local $temp
    get_global $MP
    get_local $temp
    i32.store
    get_global $MP
    get_global $SP
    i32.store offset=4
    get_global $MP
    i32.const 8
    i32.add
    set_local $localsStart

i32.const 4
get_local $localsStart 
i32.add
i32.const 0
i32.store     ;;Declaracion
block $fin
loop  $init ;;For
i32.const 4
get_local $localsStart 
i32.add
i32.load 
i32.const 3
i32.lt_s 
i32.eqz
br_if 1
i32.const 8
get_local $localsStart 
i32.add
i32.const 0
i32.store     ;;Declaracion
block $fin
loop  $init ;;For
i32.const 8
get_local $localsStart 
i32.add
i32.load 
i32.const 3
i32.lt_s 
i32.eqz
br_if 1
i32.const 0
get_local $localsStart 
i32.add
i32.load 
i32.const 12
i32.const 4
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 8
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 0
i32.store     ;;Asignacion
i32.const 12
get_local $localsStart 
i32.add
i32.const 0
i32.store     ;;Declaracion
block $fin
loop  $init ;;For
i32.const 12
get_local $localsStart 
i32.add
i32.load 
i32.const 3
i32.lt_s 
i32.eqz
br_if 1
i32.const 0
get_local $localsStart 
i32.add
i32.load 
i32.const 12
i32.const 4
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 8
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 0
get_local $localsStart 
i32.add
i32.load 
i32.const 12
i32.const 4
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 8
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.load 
i32.const 0
get_local $this 
i32.add
i32.const 12
i32.const 4
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 12
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.load 
i32.const 36
get_local $this 
i32.add
i32.const 12
i32.const 12
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 8
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.load 
i32.mul 
i32.add 
i32.store     ;;Asignacion
i32.const 12
get_local $localsStart 
i32.add
i32.const 12
get_local $localsStart 
i32.add
i32.load 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 12
get_local $localsStart 
i32.add
i32.load 
set_local $temp3
br 0
end ;;finFor
end
i32.const 8
get_local $localsStart 
i32.add
i32.const 8
get_local $localsStart 
i32.add
i32.load 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 8
get_local $localsStart 
i32.add
i32.load 
set_local $temp3
br 0
end ;;finFor
end
i32.const 4
get_local $localsStart 
i32.add
i32.const 4
get_local $localsStart 
i32.add
i32.load 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 4
get_local $localsStart 
i32.add
i32.load 
set_local $temp3
br 0
end ;;finFor
end

 i32.const -1 
 call $freeStack)
(func $printMatriz (type $_sig_ri32) (result i32)
    (local $temp i32)
 (local $temp2 i32)
 (local $temp3 i32)
  (local $localsStart i32)
   i32.const 52
 call $reserveStack  ;; returns old MP (dynamic link)
    set_local $temp
    get_global $MP
    get_local $temp
    i32.store
    get_global $MP
    get_global $SP
    i32.store offset=4
    get_global $MP
    i32.const 8
    i32.add
    set_local $localsStart

i32.const 36
get_local $localsStart 
i32.add
i32.const 0
i32.store     ;;Declaracion
block $fin
loop  $init ;;For
i32.const 36
get_local $localsStart 
i32.add
i32.load 
i32.const 3
i32.lt_s 
i32.eqz
br_if 1
i32.const 40
get_local $localsStart 
i32.add
i32.const 0
i32.store     ;;Declaracion
block $fin
loop  $init ;;For
i32.const 40
get_local $localsStart 
i32.add
i32.load 
i32.const 3
i32.lt_s 
i32.eqz
br_if 1
i32.const 0
get_local $localsStart 
i32.add
i32.const 12
i32.const 36
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 40
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.load 
call $print     ;;print
i32.const 40
get_local $localsStart 
i32.add
i32.const 40
get_local $localsStart 
i32.add
i32.load 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 40
get_local $localsStart 
i32.add
i32.load 
set_local $temp3
br 0
end ;;finFor
end
i32.const 0
i32.const 1
i32.sub 
call $print     ;;print
i32.const 36
get_local $localsStart 
i32.add
i32.const 36
get_local $localsStart 
i32.add
i32.load 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 36
get_local $localsStart 
i32.add
i32.load 
set_local $temp3
br 0
end ;;finFor
end

 i32.const -1 
 call $freeStack)
(func $main (type $_sig_void)
    (local $temp i32)
 (local $temp2 i32)
 (local $temp3 i32)
  (local $localsStart i32)
   i32.const 204
 call $reserveStack  ;; returns old MP (dynamic link)
    set_local $temp
    get_global $MP
    get_local $temp
    i32.store
    get_global $MP
    get_global $SP
    i32.store offset=4
    get_global $MP
    i32.const 8
    i32.add
    set_local $localsStart

    ;;Declaracion sin lado derecho
    ;;Declaracion sin lado derecho
i32.const 72
get_local $localsStart 
i32.add
i32.const 0
i32.store     ;;Declaracion
block $fin
loop  $init ;;For
i32.const 72
get_local $localsStart 
i32.add
i32.load 
i32.const 3
i32.lt_s 
i32.eqz
br_if 1
i32.const 76
get_local $localsStart 
i32.add
i32.const 0
i32.store     ;;Declaracion
block $fin
loop  $init ;;For
i32.const 76
get_local $localsStart 
i32.add
i32.load 
i32.const 3
i32.lt_s 
i32.eqz
br_if 1
i32.const 0
get_local $localsStart 
i32.add
i32.const 12
i32.const 72
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 76
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 3
i32.const 72
get_local $localsStart 
i32.add
i32.load 
i32.mul 
i32.const 76
get_local $localsStart 
i32.add
i32.load 
i32.add 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 36
get_local $localsStart 
i32.add
i32.const 12
i32.const 72
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 76
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 3
i32.const 3
i32.const 72
get_local $localsStart 
i32.add
i32.load 
i32.sub 
i32.mul 
i32.const 76
get_local $localsStart 
i32.add
i32.load 
i32.sub 
i32.store     ;;Asignacion
i32.const 76
get_local $localsStart 
i32.add
i32.const 76
get_local $localsStart 
i32.add
i32.load 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 76
get_local $localsStart 
i32.add
i32.load 
set_local $temp3
br 0
end ;;finFor
end
i32.const 72
get_local $localsStart 
i32.add
i32.const 72
get_local $localsStart 
i32.add
i32.load 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 72
get_local $localsStart 
i32.add
i32.load 
set_local $temp3
br 0
end ;;finFor
end
get_global $SP
 i32.const 8
 i32.add
 set_local $temp
i32.const 0
get_local $localsStart 
i32.add

 get_local $temp 
i32.const 9
 call $copyn
get_local $temp
 i32.const 36
 i32.add
 set_local $temp
call $printMatriz;; Llamada a funcion 
 set_local $temp3
get_global $SP
 i32.const 8
 i32.add
 set_local $temp
i32.const 36
get_local $localsStart 
i32.add

 get_local $temp 
i32.const 9
 call $copyn
get_local $temp
 i32.const 36
 i32.add
 set_local $temp
call $printMatriz;; Llamada a funcion 
 set_local $temp3
i32.const 72
get_local $localsStart 
i32.add
get_global $SP
 i32.const 8
 i32.add
 set_local $temp
i32.const 0
get_local $localsStart 
i32.add

 get_local $temp 
i32.const 9
 call $copyn
get_local $temp
 i32.const 36
 i32.add
 set_local $temp
i32.const 36
get_local $localsStart 
i32.add

 get_local $temp 
i32.const 9
 call $copyn
get_local $temp
 i32.const 36
 i32.add
 set_local $temp
call $mulMat;; Llamada a funcion 
 i32.store     ;;Declaracion
get_global $SP
 i32.const 8
 i32.add
 set_local $temp
i32.const 72
get_local $localsStart 
i32.add
i32.load 

 get_local $temp 
i32.const 9
 call $copyn
get_local $temp
 i32.const 36
 i32.add
 set_local $temp
call $printMatriz;; Llamada a funcion 
 set_local $temp3
i32.const 111111
call $print     ;;print
    ;;Declaracion sin lado derecho
    ;;Declaracion sin lado derecho
i32.const 184
get_local $localsStart 
i32.add
i32.const 0
i32.store     ;;Declaracion
block $fin
loop  $init ;;For
i32.const 184
get_local $localsStart 
i32.add
i32.load 
i32.const 3
i32.lt_s 
i32.eqz
br_if 1
i32.const 188
get_local $localsStart 
i32.add
i32.const 0
i32.store     ;;Declaracion
block $fin
loop  $init ;;For
i32.const 188
get_local $localsStart 
i32.add
i32.load 
i32.const 3
i32.lt_s 
i32.eqz
br_if 1
i32.const 112
get_local $localsStart 
i32.add
i32.const 0
 i32.add
i32.const 12
i32.const 184
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 188
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 3
i32.const 184
get_local $localsStart 
i32.add
i32.load 
i32.mul 
i32.const 188
get_local $localsStart 
i32.add
i32.load 
i32.add 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 112
get_local $localsStart 
i32.add
i32.const 36
 i32.add
i32.const 12
i32.const 184
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 4
i32.const 188
get_local $localsStart 
i32.add
i32.load 
i32.mul
 i32.add
i32.const 3
i32.const 3
i32.const 184
get_local $localsStart 
i32.add
i32.load 
i32.sub 
i32.mul 
i32.const 188
get_local $localsStart 
i32.add
i32.load 
i32.sub 
i32.store     ;;Asignacion
i32.const 188
get_local $localsStart 
i32.add
i32.const 188
get_local $localsStart 
i32.add
i32.load 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 188
get_local $localsStart 
i32.add
i32.load 
set_local $temp3
br 0
end ;;finFor
end
i32.const 184
get_local $localsStart 
i32.add
i32.const 184
get_local $localsStart 
i32.add
i32.load 
i32.const 1
i32.add 
i32.store     ;;Asignacion
i32.const 184
get_local $localsStart 
i32.add
i32.load 
set_local $temp3
br 0
end ;;finFor
end
get_global $SP
 i32.const 8
 i32.add
 set_local $temp
i32.const 112
get_local $localsStart 
i32.add
i32.const 0
 i32.add

 get_local $temp 
i32.const 9
 call $copyn
get_local $temp
 i32.const 36
 i32.add
 set_local $temp
call $printMatriz;; Llamada a funcion 
 set_local $temp3
get_global $SP
 i32.const 8
 i32.add
 set_local $temp
i32.const 112
get_local $localsStart 
i32.add
i32.const 36
 i32.add

 get_local $temp 
i32.const 9
 call $copyn
get_local $temp
 i32.const 36
 i32.add
 set_local $temp
call $printMatriz;; Llamada a funcion 
 set_local $temp3
i32.const 76
get_local $localsStart 
i32.add
get_global $SP
 i32.const 8
 i32.add
 set_local $temp
set_local $temp3
 get_local $temp
 get_local $temp3
 i32.store
 get_local $temp
 i32.const 4
 i32.add
 set_local $temp
i32.const 112
get_local $localsStart 
i32.add
i32.const 0
 i32.add
call $MatMult_multMat;; Llamada a funcion de objeto 
 set_local $temp3
get_global $SP
 i32.const 8
 i32.add
 set_local $temp
i32.const 76
get_local $localsStart 
i32.add

 get_local $temp 
i32.const 9
 call $copyn
get_local $temp
 i32.const 36
 i32.add
 set_local $temp
call $printMatriz;; Llamada a funcion 
 set_local $temp3

 call $freeStack)
(func $copyn (type $_sig_i32i32i32) ;; copy $n i32 slots from $src to $dest
   (param $src i32)
   (param $dest i32)
   (param $n i32)
   block
     loop
       get_local $n
       i32.eqz
       br_if 1
       get_local $n
       i32.const 1
       i32.sub
       set_local $n
       get_local $dest
       get_local $src
       i32.load
       i32.store
       get_local $dest
       i32.const 4
       i32.add
       set_local $dest
       get_local $src
       i32.const 4
       i32.add
       set_local $src
       br 0
     end
   end
))