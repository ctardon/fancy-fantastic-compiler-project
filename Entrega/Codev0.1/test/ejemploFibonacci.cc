// Ejemplo de Fibonacci. Contenidos: acceso a arrays y punteros, pasar punteros como parámetos, acceso a objetos

class Fibonacci{
    int[200] arrSol;
    
    int nesimo(int n){ // devuelve el n-ésimo número de Fibonacci
        if((n == 0) || (n == 1))
            return 1;
        int sol = nesimo(n-1) + nesimo(n-2);
        return sol;
    }
    
    void nprimeros(){ // Función que pone en arrSol los 200 primeros números de Fibonacci
        for(int k = 0; k < 200; ++k){
            if((k == 0) || (k == 1)){
                arrSol[k] = 1;
            }
            else{
                int num = arrSol[k-1] + arrSol[k-2];
                arrSol[k] = num;
            }
        }
    }
}


void main(){
    Fibonacci fib;
    int n1 = 20;
    int n2 = 10;
    int n3 = 30;
    
    int sol = fib.nesimo(n1);
    print(sol);
    print(-1); // Usamos -1 como salto de línea improvisado
    fib.nprimeros();
    for(int i = 0; i <= n1; ++i)
        print(fib.arrSol[i]);
    print(-1);
    sol = fib.nesimo(n2);
    print(sol);
    print(-1);
    for(int j = 0; j <= n2; ++j)
        print(fib.arrSol[j]);
    print(-1);
    sol = fib.nesimo(n3);
    print(sol);
    print(-1);
    for(int k = 0; k <= n3; ++k)
        print(fib.arrSol[k]);
}

