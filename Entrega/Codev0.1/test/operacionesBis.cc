// Fichero de ejemplo includer.cc. Contenido: Include, using, operaciones bis implementadas (resta y división), switch, bloques anidades (for y switch), continue y break
#include "test/pruebaInclude.cc";

using ent = int;



void main(){
    ent n = 49;
    print(n);
    ent a = 18;
    ent b = 180;
    for(int i = 0; i < 6; ++i){
        switch(i){
            case(0){
                print(a-b); // resta normal: -162
            }
            case(1){
                print(a-:b); // resta "bis" (o resta con asociativadad invertida: 162
            }
            case(2){
                print(a/b); // división normal: 0
            }
            case(4){
                print(a/:b); // división "bis" (o división con asociatividad invertida): 10
            }
            case(3){
                continue;
            }
            default{
                break;
            }
        }
        print(i);
    }
    includedVoidFunction(); // imprime 99 
    ent sol = includedIntFunction(a,b); //imprime a*b*(a+b)
    // Funciones de pruebaInclude.cc
    print(sol);
}
