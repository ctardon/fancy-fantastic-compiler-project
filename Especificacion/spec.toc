\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {spanish}{}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n al lenguaje}{2}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Requisitos}{2}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Identificadores y \IeC {\'A}mbitos de Definici\IeC {\'o}n}{2}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Tipos}{6}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Conjunto de Instrucciones del Lenguaje}{9}{subsection.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4}Gesti\IeC {\'o}n de Errores}{13}{subsection.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.1}Ejemplo de errores l\IeC {\'e}xicos}{15}{subsubsection.2.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.2}Ejemplo de errores de vinculaci\IeC {\'o}n}{15}{subsubsection.2.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.4.3}Ejemplo de errores de chequeo de tipos}{15}{subsubsection.2.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Programas de Ejemplo}{17}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Ejemplo 1: Recursi\IeC {\'o}n}{17}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Ejemplo 2: Include, operaciones aritm\IeC {\'e}ticas, using, bucles y switch}{17}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Ejemplo 3: Bucles, recursi\IeC {\'o}n y declaraci\IeC {\'o}n y llamada a funciones}{19}{subsection.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}Ejemplo 4: Bucles, arrays (creaci\IeC {\'o}n y acceso), matrices, punteros y Clase}{19}{subsection.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5}Ejemplo 5: Clases, llamada a m\IeC {\'e}todos, recursi\IeC {\'o}n en m\IeC {\'e}todos y arrays}{22}{subsection.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.6}Ejemplo 6: Todo junto}{24}{subsection.3.6}
